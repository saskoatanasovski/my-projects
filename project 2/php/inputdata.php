<?php

    require "dbconnection.php";

    if ($_SERVER["REQUEST_METHOD"] == "POST"
        && isset($_POST["cover_photo"]) && !empty($_POST["cover_photo"])
        && isset($_POST["title"]) && !empty($_POST["title"])
        && isset($_POST["subtitle"]) && !empty($_POST["subtitle"])
        && isset($_POST["about_us"]) && !empty($_POST["about_us"])
        && isset($_POST["phone"]) && !empty($_POST["phone"])
        && isset($_POST["location"]) && !empty($_POST["location"])
        && isset($_POST["servisi_producti"]) && !empty($_POST["cover_photo"])
        && isset($_POST["URL1"]) && !empty($_POST["URL1"])
        && isset($_POST["URL1_description"]) && !empty($_POST["URL1_description"])
        && isset($_POST["URL2"]) && !empty($_POST["URL2"])
        && isset($_POST["URL2_description"]) && !empty($_POST["URL2_description"])
        && isset($_POST["URL3"]) && !empty($_POST["URL3"])
        && isset($_POST["URL3_description"]) && !empty($_POST["URL3_description"])
        && isset($_POST["about_company"]) && !empty($_POST["about_company"])
        && isset($_POST["linkedin"]) && !empty($_POST["linkedin"])
        && isset($_POST["facebook"]) && !empty($_POST["facebook"])
        && isset($_POST["tweeter"]) && !empty($_POST["tweeter"])
    ) {
       
    }
    else{
        header("Location: ../create page.php? error=*Задолжително поле");
        die;
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $sql = "INSERT INTO page (cover_link,title,subtitle,about_us,phone,location,products_service,URL1,description_URL1,URL2,description_URL2,URL3,description_URL3,about_company,linkedin,facebook,tweeter)
        VALUES (:cover_photo, :title, :subtitle,:about_us, :phone, :location, :servisi_producti, :URL1, :URL1_description, :URL2, :URL2_description, :URL3, :URL3_description, :about_company, :linkedin, :facebook, :tweeter) ";
        $query = $conn->prepare($sql);
        $rezultat = $query->execute([

            'cover_photo' => $_POST["cover_photo"],
            'title' => $_POST["title"],
            'subtitle' => $_POST["subtitle"],
            'about_us' => $_POST["about_us"],
            'phone' => $_POST["phone"],
            'location' => $_POST["location"],
            'servisi_producti' => $_POST["servisi_produkti"],
            'URL1' => $_POST["URL1"],
            'URL1_description' => $_POST["URL1_description"],
            'URL2' => $_POST["URL2"],
            'URL2_description' => $_POST["URL2_description"],
            'URL3' => $_POST["URL3"],
            'URL3_description' => $_POST["URL3_description"],
            'about_company' => $_POST["about_company"],
            'linkedin' => $_POST["linkedin"],
            'facebook' => $_POST["facebook"],
            'tweeter' => $_POST["tweeter"],
        ]);
        

       
       

        $sql2 = "SELECT id FROM page WHERE title = '{$_POST['title']}'";
        $query2 = $conn->query($sql2);
        $rezult2 = $query2->fetch();
        
        header("Location: ../final.php?id={$rezult2['id']}");
        die;

       
    }


?>