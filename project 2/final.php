
<?php

    require "./php/getdata.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="nav">
        <ul>
            <li>
                <a href="index.html">Дома</a>
            </li>
            <li>
                <a href="#about_us">За нас</a>
            </li>
            <li>
                <a href="#service">Сервиси или Продукти</a>
            </li>
            <li>
                <a href="#contact">Контакт</a>
            </li>
        </ul>
    </div>
       
    <div class="header" style="background-image: url(<?php echo $row['cover_link']?>);">
        <h1><?php echo $row["title"]?></h1>
        <h3><?php echo $row["subtitle"]?></h3>
    </div>
    <div id="about_us"></div>
    <div  class="about_us">
        <div class="about_us_left">
            <h2>За нас</h2>
            <p><?php echo $row["about_us"]?></p>
        </div>
        <div class="about_us_right">
            <p>Телефон</p>
            <p><?php echo $row["phone"]?></p>
            <p>Локација</p>
            <p><?php echo $row["location"]?></p>

        </div>
    </div>
    <div id="service"></div>
    <div  class="service">
        <div class="service_left">
            <img src="<?php echo $row["URL1"]?>" alt="service">
            <h3>Опис на првиот <?php echo $row["products_service"]?></h3>
            <p><?php echo $row["description_URL1"]?></p>
        </div>
        <div class="service_middle">
            <img src="<?php echo $row["URL2"]?>" alt="service">
            <h3>Опис на првиот <?php echo $row["products_service"]?></h3>
            <p><?php echo $row["description_URL2"]?></p>
        </div>
        <div class="service_right">
            <img src="<?php echo $row["URL3"]?>" alt="service">
            <h3>Опис на првиот <?php echo $row["products_service"]?></h3>
            <p><?php echo $row["description_URL3"]?></p>
        </div>
    </div>
    <div id="contact"></div>
    <h2  class="contact_title">Контакт</h2>
    <div class="contact">
       
        <div class="contact_left">
            <h3>За нашата компанија</h3>
            <p><?php echo $row["about_company"]?></p>
        </div>
        <div class="contact_right">
            <form action="">
                <label for="name">Име</label>
                <input type="text" name="name" id="name" placeholder="Вашето име">
                <label for="email">Емаил</label>
                <input type="email" name="email" id="email" placeholder="Вашиот емаил">
                <label for="poraka">Порака</label>
                <textarea name="poraka" id="poraka" rows="5"></textarea>
                <div class="button_cont">
                    <button type="submit">Испрати</button>
                </div>
            </form>
        </div>
    </div>
    <div class="footer">
        <div class="footer_left">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro aspernatur eius dolorem. Dolore doloribus consequuntur provident, voluptatibus laborum accusamus modi vero itaque ab inventore placeat reiciendis id veritatis recusandae possimus.</p>
        </div>
        <div class="footer_right">
            <a href="<?php echo $row['linkedin']?>" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
            <a href="<?php echo $row['facebook']?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
            <a href="<?php echo $row['tweeter']?>" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
        </div>
       
    </div>  
                               
</body>

</html>