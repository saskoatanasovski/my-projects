
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Page</title>
    <style>
        body {
            margin: 0 auto;
            background-color: #7195ff;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            padding: 10%;
        }

        .container {
            width: 100%;
            
        }

        input,
        textarea,
        select {
            display: block;
            width: 100%;
            margin: 1% 0 5%;
            border-radius: 3px;
        }

        input,
        select {
            height: 30px;
        }
       
        .line {
            background-color: white;
            height: 2px;
            width: 100%;
            margin-bottom: 5%;
        }
        .button_cont{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            margin-bottom: 5%;
        }
        button{
            background-color: blue;
            color: white;
            padding: 14px;
            border-radius: 5px;
            font-size: 18px;
            cursor: pointer;
            margin: auto;
        }
        .text-red{
            color: red;
        }
        @media screen and (min-width: 769px){
            body{
                padding: 0;
            }

            .container {
            width: 40%;
            }
            
            
        }
    </style>
</head>

<body>

    <div class="container">

        <h2>Еден чекор ве дели од вашата веб страница</h2>

        <form action="php/inputdata.php" method="POST">

            <label for="cover_photo">Напишете го линкот до cover сликата:</label>
            <input id="cover_photo" name="cover_photo" type="text">
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <label for="title">Внесете го насловот:</label>
            <input id="title" name="title" type="text">
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <label for="subtitle">Внесете го поднасловот:</label>
            <input id="subtitle" name="subtitle" type="text" >
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <label for="about">Напишете нешто за вас:</label>
            <textarea name="about_us" id="about" rows="5"></textarea>
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <label for="phone">Внесете го вашиот телефон:</label>
            <input id="phone" name="phone" type="text">
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <label for="location">Внесете ја вашата локација:</label>
            <input id="location" name="location" type="text">
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <div class="line"></div>

            <label>Одберете дали нудите сервиси или продукти:</label>
            <select name="servisi_produkti" id="offer">
                <option selected disabled></option>
                <option value="сервис">Сервиси</option>
                <option value="продукт">Продукти</option>
            </select>
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <p>Внесете URL од слика од вашите продукти или сервиси:</p>

            <label for="product_photo1">URL од слика</label>
            <input id="product_photo1" name="URL1" type="text">
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <label for="product_photo_description1">Опис за сликата</label>
            <textarea name="URL1_description" id="product_photo_description1" rows="5"></textarea>
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <label for="product_photo2">URL од слика</label>
            <input id="product_photo2" name="URL2" type="text">
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <label for="product_photo_description2">Опис за сликата</label>
            <textarea name="URL2_description" id="product_photo_description2" rows="5"></textarea>
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <label for="product_photo3">URL од слика</label>
            <input id="product_photo3" name="URL3" type="text">
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <label for="product_photo_description3">Опис за сликата</label>
            <textarea name="URL3_description" id="product_photo_description3" rows="5"></textarea>
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <div class="line"></div>

            <label for="company_description">Напишете нешто за вашата фирма што луѓето треба да го знаат пред да ве контактираат:</label>
            <textarea name="about_company" id="company_description" rows="5"></textarea>
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <div class="line"></div>

            <label for="lionkedin">Linkedin</label>
            <input id="linkedin" name="linkedin" type="text">
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <label for="facebook">Facebook</label>
            <input id="facebook" name="facebook" type="text">
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <label for="tweeter">Twitter</label>
            <input id="tweeter" name="tweeter" type="text">
            <?php if (isset($_GET['error'])){?>
                <p class="text-red"><?php echo $_GET['error']?></p>
            <?php } ?>

            <div class="line"></div>

            <div class="button_cont">
                <button type="submit">Потврди</button>
            </div>
        </form>
    </div>

</body>

</html>