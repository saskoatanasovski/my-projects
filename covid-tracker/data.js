
    // on load
    fetch('php/printCountries.php')
    .then(function(response){
        return response.json();
    })
    .then(function(body){
        let countrySet = document.querySelector("#countrySel");
        
        body.forEach(element => {
        let content =` <option value="${element.name}"> ${element.name}</option>`;
        countrySet.insertAdjacentHTML("beforeend", content);
        });
    })
    .catch(function(err){
        console.log('err c',err);
    })

    fetch('php/printTable.php')
    .then(function(response){
        return response.json();
    })
    .then(function(body){
        
        let tdata = document.querySelector("#tdata");
        
        let content='';
        body.forEach(element => {
            content +=
            `
                <tr>
                    <td>${element.country}</td>
                    <td>${element.date}</td>
                    <td>${element.code}</td>
                    <td><img class="flag" src="${element.flag}"></td>
                    <td>${element.newConfirmed}</td>
                    <td>${element.newDeaths}</td>
                    <td>${element.newRecovered}</td>
                    <td>${element.totalConfirmed}</td>
                    <td>${element.totalDeaths}</td>
                    <td>${element.totalRecovered}</td>
			<td>${element.totalActive}</td>
                   
                </tr>
            `
        });
        tdata.innerHTML = content;
    })
    .catch(function(err){
        console.log('err t',err);
    })

    fetch('php/printGlobal.php')
    .then(function(response){
        return response.json();
    })
    .then(function(data){
        let generalConfirmed =  document.querySelector("#generalConfirmed");
        let generalDeaths = document.querySelector("#generalDeaths");
        let generalRecovered = document.querySelector("#generalRecovered");
       
        generalConfirmed.innerHTML=
            `
            <h5 class="text-center text-secondary">New Confirmed:</h5>
            <h3 class="text-center mb-3 text-secondary">${data.newConfirmed}</h3>
            <h5 class="text-center">Total Confirmed: ${data.totalConfirmed}</h5>
            `
        ;
        generalDeaths.innerHTML =
            `
            <h5 class="text-center text-danger">New Deaths: </h5>
            <h3 class="text-center mb-3 text-danger">${data.newDeaths}</h3>
            <h5 class="text-center">Total Death: ${data.totalDeaths}</h5>
            `
        ;
        generalRecovered.innerHTML=
            `
            <h5 class="text-center text-success">New Recovered: </h5>
            <h3 class="text-center mb-3 text-success">${data.newRecovered}</h3>
            <h5 class="text-center">Total Recovered: ${data.totalRecovered}</h5>
            `
        ;
        let ctx = document.getElementById('myChart');
        let myChart = new Chart(ctx, {
        type: 'bar',
            data: {
                labels: ['Deaths', 'Confirmed', 'Recovered'],
                datasets: [{
                    label: 'Daily Cases',
                   
                    data: [data.newDeaths,data.newConfirmed,data.newRecovered],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(75, 192, 192, 1)',
                    ],
                    borderWidth: 1
                }]
            },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
        });
        let ctx2 = document.getElementById('myChart2');
        let myChart2 = new Chart(ctx2, {
            type: 'bar',
                data: {
                    labels: ['Deaths', 'Confirmed', 'Recovered'],
                    datasets: [{
                        label: 'Total Cases',
                        data: [data.totalDeaths,data.totalConfirmed,data.totalRecovered],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(75, 192, 192, 1)',
                        ],
                        borderWidth: 1
                    }]
                },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
        });
    })
    .catch(function(err){
        console.log('err g',err);
    })
    // on load end
    // sinch data
    let synchBtn = document.querySelector("#synch");
    synchBtn.addEventListener("click",function(e){
        data = new FormData();
        data.append('synchronise','synchronise');
        fetch('php/updateGlobal.php',{
            method: "POST",
            body: data
        })
        .then(function(response){
            console.log(response)
            return response.json()
        })
        .then(function(data){
            console.log(data)
        })
        .catch(function(err){
            console.log('err',err);
        })
        fetch('php/updateDataCases.php',{
            method: "POST",
            body: data
        })
        .then(function(response){
            console.log(response)
            return response.json()
        })
        .then(function(data){
            console.log(data)
        })
        .catch(function(err){
            console.log('err',err);
        })

        
    })
    // synch data end
    // select country
    let countrySet = document.querySelector("#selectForm")
    countrySet.addEventListener("submit",function(e){
        e.preventDefault();
       
        let countryVal = document.querySelector("#countrySel").value;
        let period = document.querySelector("#period").value;
        data = new FormData();
        
            data.append("country",countryVal);
            data.append("period",period);
     
        fetch('php/selectCountryData.php',{
            method: "POST",
            body: data
            
        })
        .then(function(response){
            console.log(response)
            return response.json()
        })
        .then(function(data){
           
            console.log(data)
            let generalConfirmed =  document.querySelector("#generalConfirmed");
            let generalDeaths = document.querySelector("#generalDeaths");
            let generalRecovered = document.querySelector("#generalRecovered");
            let generalTitle = document.querySelector("#generalTitle");
            data.forEach(element=>{
                if (element.country && element.newConfirmed) {
               
                    generalTitle.innerHTML = ` <h3 class=" text-white text-center p-0 m-0">COVID-19 status for ${element.country} for today</h3>`;
                    generalConfirmed.innerHTML=
                    `
                        <h5 class="text-center text-secondary">New Confirmed:</h5>
                        <h3 class="text-center mb-3 text-secondary">${element.newConfirmed}</h3>
                        <h5 class="text-center">Total Confirmed: ${element.confirmed}</h5>
                    `
                    ;
                    generalDeaths.innerHTML =
                        `
                        <h5 class="text-center text-danger">New Deaths: </h5>
                        <h3 class="text-center mb-3 text-danger">${element.newDeaths}</h3>
                        <h5 class="text-center">Total Death: ${element.deaths}</h5>
                        `
                    ;
                    generalRecovered.innerHTML=
                        `
                        <h5 class="text-center text-success">New Recovered: </h5>
                        <h3 class="text-center mb-3 text-success">${element.newRecovered}</h3>
                        <h5 class="text-center">Total Recovered: ${element.recovered}</h5>
                        `
                    ;
                    let titleDaily = `Daily Cases for ${element.country}`;
                    let titleTotal =  `Total Cases for ${element.country}`;
                    let ctx = document.getElementById('myChart');
                    let myChart = new Chart(ctx, {
                    type: 'bar',
                        data: {
                            labels: ['Deaths', 'Confirmed', 'Recovered'],
                            datasets: [{
                                label: titleDaily,
                            
                                data: [element.newDeaths,element.newConfirmed,element.newRecovered],
                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(54, 162, 235, 0.2)',
                                    'rgba(75, 192, 192, 0.2)',
                                ],
                                borderColor: [
                                    'rgba(255, 99, 132, 1)',
                                    'rgba(54, 162, 235, 1)',
                                    'rgba(75, 192, 192, 1)',
                                ],
                                borderWidth: 1
                            }]
                        },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                    });
                    let ctx2 = document.getElementById('myChart2');
                    let myChart2 = new Chart(ctx2, {
                        type: 'bar',
                            data: {
                                labels: ['Deaths', 'Confirmed', 'Recovered'],
                                datasets: [{
                                    label: titleTotal,
                                    data: [element.deaths,element.confirmed,element.recovered],
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                    ],
                                    borderColor: [
                                        'rgba(255, 99, 132, 1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(75, 192, 192, 1)',
                                    ],
                                    borderWidth: 1
                                }]
                            },
                                options: {
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }
                    });
                    console.log(element)
               
                }else if(!element.country){
                    let charts = document.querySelector("#charts");
                    let titleTotal =  `Total Cases for ${period} days`;
                    generalTitle.innerHTML = ` <h3 class=" text-white text-center p-0 m-0">COVID-19 status for ${period} days</h3>`;
                    generalConfirmed.innerHTML=
                    `
                        <h5 class="text-center text-secondary">Total Confirmed:</h5>
                        <h3 class="text-center mb-3 text-secondary">${element.confirmed}</h3>
                        
                    `
                    ;
                    generalDeaths.innerHTML =
                        `
                        <h5 class="text-center text-danger">Total Deaths: </h5>
                        <h3 class="text-center mb-3 text-danger">${element.deaths}</h3>
                       
                        `
                    ;
                    generalRecovered.innerHTML=
                        `
                        <h5 class="text-center text-success">Totoal Recovered: </h5>
                        <h3 class="text-center mb-3 text-success">${element.recovered}</h3>
                       
                        `
                    ;
                    let content = `
                        <h3 class="text-white text-center my-4">COVID-19 statistic</h3>
                        <div class="col">
                            <canvas class="bg-light shadow-selects mb-4 mb-md-0 border-2 border-dark" id="myChart2" width="100%" height="40%"></canvas>
                        </div>

                    `
                    charts.innerHTML = content;

                    let ctx2 = document.getElementById('myChart2');
                    let myChart2 = new Chart(ctx2, {
                        type: 'bar',
                            data: {
                                labels: ['Deaths', 'Confirmed', 'Recovered'],
                                datasets: [{
                                    label: titleTotal,
                                    data: [element.deaths,element.confirmed,element.recovered],
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                    ],
                                    borderColor: [
                                        'rgba(255, 99, 132, 1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(75, 192, 192, 1)',
                                    ],
                                    borderWidth: 1
                                }]
                            },
                                options: {
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }
                    });
                    console.log(element)
                
                }else if(element.country && element.active){
                    let charts = document.querySelector("#charts");
                    let titleTotal =  `Total Cases for ${element.country} for ${period} days period`;
               
                    generalTitle.innerHTML = ` <h3 class=" text-white text-center p-0 m-0">COVID-19 status for ${element.country} for ${period} days period</h3>`;
                    generalConfirmed.innerHTML=
                    `
                        <h5 class="text-center text-secondary">Total Confirmed:</h5>
                        <h3 class="text-center mb-3 text-secondary">${element.confirmed}</h3>
                        
                    `
                    ;
                    generalDeaths.innerHTML =
                        `
                        <h5 class="text-center text-danger">Total Deaths: </h5>
                        <h3 class="text-center mb-3 text-danger">${element.deaths}</h3>
                       
                        `
                    ;
                    generalRecovered.innerHTML=
                        `
                        <h5 class="text-center text-success">Totoal Recovered: </h5>
                        <h3 class="text-center mb-3 text-success">${element.recovered}</h3>
                       
                        `
                    ;
                    
                    let content = `
                        <h3 class="text-white text-center my-4">COVID-19 statistic</h3>
                        <div class="col">
                            <canvas class="bg-light shadow-selects mb-4 mb-md-0 border-2 border-dark" id="myChart2" width="100%" height="40%"></canvas>
                        </div>

                    `
                    charts.innerHTML = content;

                    let ctx2 = document.getElementById('myChart2');
                    let myChart2 = new Chart(ctx2, {
                        type: 'bar',
                            data: {
                                labels: ['Deaths', 'Confirmed', 'Recovered'],
                                datasets: [{
                                    label: titleTotal,
                                    data: [element.deaths,element.confirmed,element.recovered],
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                    ],
                                    borderColor: [
                                        'rgba(255, 99, 132, 1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(75, 192, 192, 1)',
                                    ],
                                    borderWidth: 1
                                }]
                            },
                                options: {
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }
                    });
                    console.log(element)
                }
            })
            
            
        
        })
        .catch(function(err){
            console.log('err',err);
        })
        
    })
    // select country end
    // select continent

    let continentBtn = document.querySelectorAll(".continentBtn")
    
    continentBtn.forEach(element=>{
        element.addEventListener("click",function(e){
            let text = element.innerText;
            data = new FormData();
            data.append("continent",text);
            fetch('php/printTableByContinents.php',{
                method: "POST",
                body: data
                
            })
            .then(function(response){
                
                return response.json()
            }).then(function(data){
               
                let tdata = document.querySelector("#tdata")
                let tableTitle = document.querySelector("#tableTitle");
                let innerT = `Detailed results for ${text}`;
                tableTitle.innerText = innerT;
                let content='';
                data.forEach(element => {
                    content +=
                    `
                        <tr>
                            <td>${element.country}</td>
                            <td>${element.date}</td>
                            <td>${element.code}</td>
                            <td><img class="flag" src="${element.flag}"></td>
                            <td>${element.newConfirmed}</td>
                            <td>${element.newDeaths}</td>
                            <td>${element.newRecovered}</td>
                            <td>${element.totalConfirmed}</td>
                            <td>${element.totalDeaths}</td>
                            <td>${element.totalRecovered}</td>
				<td>${element.totalActive}</td>
                        
                        </tr>
                    `
                });
                tdata.innerHTML = content;
            })
            .catch(function(err){
                console.log(err)
            })
        })
    })
    let allBtn = document.querySelector("#allBtn");
    allBtn.addEventListener("click",function(e){
        fetch('php/printTable.php')
    .then(function(response){
        return response.json();
    })
    .then(function(body){
        
        let tdata = document.querySelector("#tdata");
        let active = document.querySelector("#active");
        active.style.display = "none"
        let tableTitle = document.querySelector("#tableTitle");
        let innerT = `Detailed results by country`;
        tableTitle.innerText = innerT;
        let content='';
        body.forEach(element => {
            content +=
            `
                <tr>
                    <td>${element.country}</td>
                    <td>${element.date}</td>
                    <td>${element.code}</td>
                    <td><img class="flag" src="${element.flag}"></td>
                    <td>${element.newConfirmed}</td>
                    <td>${element.newDeaths}</td>
                    <td>${element.newRecovered}</td>
                    <td>${element.totalConfirmed}</td>
                    <td>${element.totalDeaths}</td>
                    <td>${element.totalRecovered}</td>
                   <td>${element.totalActive}</td>
                </tr>
            `
        });
        tdata.innerHTML = content;
    })
    })
    // select continent end
