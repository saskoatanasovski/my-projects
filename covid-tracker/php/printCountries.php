<?php

    require 'dbconnection.php';
    
    $sql = 'SELECT * FROM countries ORDER BY name';
    
    $query = $conn->query($sql);
    
    $countries = [];
    while ($row = $query->fetch()) {
        $content = ['name'=>$row['name'],'flag'=>$row['flag'],'code'=>$row['code']];
        array_push($countries,$content);
    }
    echo json_encode($countries);
  
    
    
?>