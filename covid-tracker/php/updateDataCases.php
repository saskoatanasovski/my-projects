<?php
require "./dbconnection.php";



$sqlCountry = "SELECT * FROM `countries`";
$stmtCountry = $conn->query($sqlCountry);



$DBData = 'SELECT * FROM cases_by_country ORDER BY date DESC LIMIT 1';
$stmDBData = $conn->query($DBData);
$dataB = $stmDBData->fetch();
$dateDB = $dataB['date'];
$curent = strval(date("Y-m-d"));

if ( $dateDB != $curent) {
    while($row=$stmtCountry->fetch(PDO::FETCH_ASSOC)){
        $country_id = $row['id'];
        
        $data = file_get_contents("https://api.covid19api.com/country/{$row['slug']}?from={$dateDB}T00:00:00Z&to={$curent}T00:00:00Z");
        
        $data = json_decode($data);
        
       
      
        $sqlCasesInsert = "INSERT INTO cases_by_country(year,month,day,date,total_confirmed,total_deaths,total_recovered,active,country_id)
        VALUES (:year,:month,:day,:date,:total_confirmed,:total_deaths,:total_recovered,:active,:country_id)";
        $query = $conn->prepare($sqlCasesInsert);

        $query->bindParam('year',$dateY);
        $query->bindParam('month',$dateM);
        $query->bindParam('day',$dateD);
        $query->bindParam('date',$dateApi);
        $query->bindParam('total_confirmed',$totalConfirmed);
        $query->bindParam('total_deaths',$totalDeaths);
        $query->bindParam('total_recovered',$totalRecovered);
        $query->bindParam('active',$active);
        $query->bindParam('country_id',$country_id);



            foreach ($data as  $value) {
                $totalConfirmed = $value->Confirmed;
                $totalDeaths = $value->Deaths;
                $totalRecovered = $value->Recovered;
                $active = $value->Active;
                $dateApi = date("Y-m-d", strtotime($value->Date)); 
                $dateY = date("Y",strtotime($value->Date));
                $dateM = date("m",strtotime($value->Date));
                $dateD = date("d",strtotime($value->Date));
    
             
                $query->execute();
            };
      
            
        sleep(0.5);
    }

}



echo json_encode("Done!");
?>