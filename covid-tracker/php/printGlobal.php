<?php

    require "./dbconnection.php";

    $sql = 'SELECT * FROM global ORDER BY date DESC LIMIT 1';
    $query = $conn->query($sql);
    $data = $query->fetch();

   
    $response=array(
        "newConfirmed"=>$data['new_confirmed'],
        "newDeaths"=>$data['new_deaths'],
        "newRecovered"=>$data['new_recovered'],
        "totalConfirmed"=>$data['total_confirmed'],
        "totalDeaths"=>$data['total_deaths'],
        "totalRecovered"=>$data['total_recovered'],
    );
  
    echo json_encode($response);
  
?>