<?php

$host = "localhost";
$username = "root";
$password = "";
$dbname = "covid_tracker";
try {
    $assoc = [PDO::ATTR_DEFAULT_FETCH_MODE=>PDO::FETCH_ASSOC];
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password,$assoc);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
    die;
}


?>