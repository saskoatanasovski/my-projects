<?php

    require "./dbconnection.php";
   
    $sql = "SELECT * FROM global ORDER BY date DESC LIMIT 1";
    $query = $conn->query($sql);
    $db = $query->fetch();
    $data = file_get_contents("https://api.covid19api.com/summary");
    $data = json_decode($data);
    $dataA = $data->Global;
    $dateDBG = $db['date'];
    $curent = strval(date("Y-m-d"));
    $dateA = strval(date("Y-m-d",strtotime($dataA->Date))); 
    
    
    if ($dateDBG != $curent && $dateDBG != $dateA) {
        $insertCountry = "INSERT INTO global (year,month,day,date,new_confirmed,new_deaths,new_recovered,total_confirmed,total_deaths,total_recovered)
        VALUES(:year,:month,:day,:date,:new_confirmed,:new_deaths,:new_recovered,:total_confirmed,:total_deaths,:total_recovered)";
        $query = $conn->prepare($insertCountry);
        $query->bindParam('year',$year);
        $query->bindParam('month',$month);
        $query->bindParam('day',$day);
        $query->bindParam('date',$date);
        $query->bindParam('new_confirmed',$new_confirmed);
        $query->bindParam('new_deaths',$new_deaths);
        $query->bindParam('new_recovered',$new_recovered);
        $query->bindParam('total_confirmed',$total_confirmed);
        $query->bindParam('total_deaths',$total_deaths);
        $query->bindParam('total_recovered',$total_recovered);
        
    
            $date = date("Y-m-d",strtotime($dataA->Date));
            $year = date("Y",strtotime($dataA->Date));
            $month = date("m",strtotime($dataA->Date));
            $day = date("d",strtotime($dataA->Date));
            $new_confirmed = $dataA->NewConfirmed;
            $new_deaths = $dataA->NewDeaths;
            $new_recovered = $dataA->NewRecovered;
            $total_confirmed = $dataA->TotalConfirmed;
            $total_deaths = $dataA->TotalDeaths;
            $total_recovered = $dataA->TotalRecovered;
            
            
    
        
        $query->execute();
    }
    $DBData = 'SELECT * FROM cases_daily ORDER BY date DESC LIMIT 1';
    $stmDBData = $conn->query($DBData);
    $dataB = $stmDBData->fetch();
    $dateDB = $dataB['date'];
    
    $sqlCountry = "SELECT * FROM `cases_daily`";
    $stmtCountry = $conn->query($sqlCountry);


    if ( $dateDB != $curent) {
       
           
            
            $data2 = file_get_contents("https://api.covid19api.com/summary");
            $data2 = json_decode($data2);
            $dataAp = $data2->Countries;
       
        if ($dateDB != $dateAp) {
            $sqlCasesCountries = "INSERT INTO cases_daily(year,month,day,date,new_confirmed,new_deaths,new_recovered,total_confirmed,total_deaths,total_recovered,country_slug)
            VALUES (:year,:month,:day,:date,:new_confirmed,:new_deaths,:new_recovered,:total_confirmed,:total_deaths,:total_recovered,:country_slug)
            ";
            $query = $conn->prepare($sqlCasesCountries);

            $query->bindParam('year',$dateY);
            $query->bindParam('month',$dateM);
            $query->bindParam('day',$dateD);
            $query->bindParam('date',$dateApi);
            $query->bindParam('new_confirmed',$newConfirmed);
            $query->bindParam('new_deaths',$newDeaths);
            $query->bindParam('new_recovered',$newRecovered);
            $query->bindParam('total_confirmed',$totalConfirmed);
            $query->bindParam('total_deaths',$totalDeaths);
            $query->bindParam('total_recovered',$totalRecovered);
            $query->bindParam('country_slug',$apiSlug);



                foreach ($dataAp as  $value) {

                    $newConfirmed = $value->NewConfirmed;
                    $newDeaths = $value->NewDeaths;
                    $newRecovered = $value->NewRecovered;
                    $totalConfirmed = $value->TotalConfirmed;
                    $totalDeaths = $value->TotalDeaths;
                    $totalRecovered = $value->TotalRecovered;
                    $dateApi = date("Y-m-d", strtotime($value->Date));
                    $dateY = date("Y",strtotime($value->Date));
                    $dateM = date("m",strtotime($value->Date));
                    $dateD = date("d",strtotime($value->Date));
                    $apiSlug = $value->Slug;
                
                    $query->execute();
                };
       
                
            sleep(0.5);
        }

    }

    echo json_encode( "Done!!!");
