<?php
    require './dbconnection.php';
    
    $dataQ = "SELECT * FROM cases_daily ORDER BY date DESC LIMIT 1";
    $stm = $conn->query($dataQ);
    $lastInput = $stm->fetch();
    $lastDate = $lastInput['date'];
    
    $sql = "SELECT countries.name,continents.name AS continent,countries.code,countries.flag, cases_daily.date,cases_daily.new_confirmed,cases_daily.new_deaths,cases_daily.new_recovered, cases_daily.total_confirmed,cases_daily.total_deaths,cases_daily.total_recovered FROM `countries`
    LEFT JOIN cases_daily ON countries.slug = cases_daily.country_slug
    LEFT JOIN continents ON continents.id = countries.continent_id
    WHERE cases_daily.date = '{$lastDate}' AND continents.name = '{$_POST['continent']}'";
    $query = $conn->query($sql);
    $container=[];
    while($data = $query->fetch()){
        
        
            $content = array(
                    'country'=> $data['name'],
                    'continent'=>$data['continent'],
                    'code'=>$data['code'],
                    'flag'=>$data['flag'],
                    'date'=>$data['date'],
                    'newConfirmed'=>$data['new_confirmed'],
                    'newDeaths'=>$data['new_deaths'],
                    'newRecovered'=>$data['new_recovered'],
                    'totalConfirmed'=>$data['total_confirmed'],
                    'totalDeaths'=>$data['total_deaths'],
                    'totalRecovered'=>$data['total_recovered'],
                );
        array_push($container,$content);
    } 

    
    echo json_encode($container);
   

?>