<?php

use Facade\Ignition\QueryRecorder\Query;

require './dbconnection.php';
    
    $container = [];
    
    
    if (isset($_POST['country']) && 
    !empty($_POST['country']) && 
    isset($_POST['period']) && 
    !empty($_POST['period']) &&
    $_POST['period'] != ""
    ) {
       
        $base = "SELECT * FROM cases_by_country
        LEFT JOIN countries ON countries.id = cases_by_country.country_id
        WHERE countries.name = '{$_POST['country']}'
        ORDER BY date DESC LIMIT 1";
        $stm = $conn->query($base);
        $rawData = $stm->fetch();
        $sql1 = "SELECT 
                    countries.name ,
                    countries.name ,
                    cases_by_country.active,cases_by_country.total_deaths,cases_by_country.total_recovered,cases_by_country.total_confirmed,
                    date_sub(curdate(),interval {$_POST['period']} day)  
                    FROM cases_by_country 
                    LEFT JOIN countries ON countries.id = cases_by_country.country_id
                    WHERE date = date_sub(curdate(),interval {$_POST['period']} day) AND countries.name = '{$_POST['country']}'";

            $query1 =$conn->query($sql1);
            $data1 = $query1->fetch();

            $active = $rawData['active'] - $data1['active'];
            $total_confirmed = $rawData['total_confirmed'] - $data1['total_confirmed'];
            $total_deaths = $rawData['total_deaths'] - $data1['total_deaths'];
            $total_recovered = $rawData['total_recovered'] - $data1['total_recovered'];
            $content=array(
                'country'=>$data1['name'],
                'active'=>$active,
                'deaths'=>$total_deaths,
                'recovered'=> $total_recovered,
                'confirmed'=>$total_confirmed,
                
            );
            array_push($container,$content); 
    }else if (isset($_POST['period']) && !empty($_POST['period'])) {
        $base = "SELECT * FROM global
        ORDER BY date DESC LIMIT 1";
        $stm = $conn->query($base);
        $rawData = $stm->fetch();

        $sql = "SELECT 
        active,total_deaths,total_recovered,total_confirmed,
        date_sub(curdate(),interval '{$_POST['period']}' day)  
        FROM cases_by_country 
        WHERE date = date_sub(curdate(),interval '{$_POST['period']}' day)";
                
            $query =$conn->query($sql);
            $data = $query->fetch();
            
            $total_confirmed = $rawData['total_confirmed'] - $data['total_confirmed'];
            $total_deaths = $rawData['total_deaths'] - $data['total_deaths'];
            $total_recovered = $rawData['total_recovered'] - $data['total_recovered'];
            $content=array(
            
            
                'deaths'=>$total_deaths,
                'recovered'=> $total_recovered,
                'confirmed'=>$total_confirmed,
                
            );
            array_push($container,$content);
            
         
    }else if (isset($_POST['country']) && !empty($_POST['country'])) {
        $sql = "SELECT * FROM `cases_daily`
                LEFT JOIN countries ON cases_daily.country_slug = countries.slug
                WHERE countries.name = '{$_POST['country']}'
                ORDER BY date DESC LIMIT 1";
        $query = $conn->query($sql);
    
        while($row = $query->fetch()){
       
            $content = array(
                'country'=> $row['name'],
                'code'=>$row['code'],
                'flag'=>$row['flag'],
                'date'=>$row['date'],
                'newConfirmed'=>$row['new_confirmed'],
                'newDeaths'=>$row['new_deaths'],
                'newRecovered'=>$row['new_recovered'],
                'confirmed'=>$row['total_confirmed'],
                'deaths'=>$row['total_deaths'],
                'recovered'=>$row['total_recovered'],
            );
            array_push($container,$content);
        }   
            
    }
    
   
   
    echo json_encode($container);
   
?>