@extends('layouts.app')
@section('title','home')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{Session::get('success')}}
                </div>
            @endif
            <h1>Welcome {{ Auth::user()->name }}</h1>
        </div>
    </div>
</div>
@endsection
