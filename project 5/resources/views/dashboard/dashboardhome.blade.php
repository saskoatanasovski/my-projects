@extends('layouts.app')
@section('title','dashboard')
@section('content')

<div class="row mt-5">
    <div class="col">
        @if (Session::has('success'))
        <div class="alert alert-success" role="alert">
           {{Session::get('success')}}
          </div>
        @endif
        <a class="btn btn-primary float-right" href="{{ route('create.user') }}">Add new user</a>
    </div>
</div>
<div class="row my-5">
    <div class="col">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Email Verified At</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->email_verified_at }}</td>
                    <td>
                        <a class="btn btn-info" href="{{ route('edit.user', $user->id) }}"><i class="far fa-edit"></i></a>
                        <a class="btn btn-danger" href="{{ route('delete.user',$user->id) }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </td>
                   
                </tr>
                @endforeach
                
              
            </tbody>
        </table>
    </div>
</div>


@endsection
