<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');

Route::middleware('auth', 'admin')->group(function (){

    Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('home.dashboard');
    Route::get('/dashboard/create', [App\Http\Controllers\DashboardController::class, 'createUser'])->name('create.user');
    Route::post('/dashboard/store', [App\Http\Controllers\DashboardController::class, 'storeUser'])->name('store.user');
    Route::get('/dashboard/edit/{id}', [App\Http\Controllers\DashboardController::class, 'editUser'])->name('edit.user');
    Route::post('/dashboard/update/{id}', [App\Http\Controllers\DashboardController::class, 'updateUser'])->name('update.user');
    Route::get('/dashboard/delete/{id}', [App\Http\Controllers\DashboardController::class, 'deleteUser'])->name('delete.user');
    Route::get('/dashboard/verify/{user}', [App\Http\Controllers\DashboardController::class, 'verifyUser'])->name('verify.user');
    Route::get('/dashboard/resend/{id}', [App\Http\Controllers\DashboardController::class, 'resendCode'])->name('resend.code');

});

