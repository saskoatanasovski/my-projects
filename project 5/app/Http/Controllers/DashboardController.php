<?php

namespace App\Http\Controllers;

use App\Events\WelcomeMailEvent;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\VerificationCodeModel;
use Str;
class DashboardController extends Controller
{
    public function index(){

        $users = User::all();

        return view('dashboard.dashboardhome',['users'=>$users]);
    }

    public function createUser(){

        return view('dashboard.create');

    }

    public function storeUser(Request $req){
       
       $req->validate([
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:8'],
       ]);
        $user = new User;
        $user->name = $req->input('name');
        $user->email = $req->input('email');
        $user->password = Hash::make($req->input('password'));
        $user->save();
    

       if ($req->input('admin') == 'on') {
           $user->is_admin = true;
           $user->save();
        }
        else{
            $user->is_admin = false;
            $user->save();
        }
        $user_code = User::where('email',$req->input('email'))->first();

        VerificationCodeModel::create([
            'user_id'=>$user_code->id,
            'code'=>Str::random(80),
        ]);

        event(new WelcomeMailEvent($user));
        
        return redirect()->route('home.dashboard')->with('success','New user was successfully added');
    }

    public function editUser($id){
        $user = User::where('id',$id)->first();
        
        return view('dashboard.edit',['user'=>$user]);
    }

    public function updateUser(Request $req,$id){
       
        $req->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
           ]);
            $user =  User::find($id);
            $user->name = $req->input('name');
            $user->email = $req->input('email');
            $user->email_verified_at = NULL;
            $user->password = Hash::make($req->input('password'));
            $user->update();
        
    
           if ($req->input('admin') == 'on') {
               $user->is_admin = true;
               $user->update();
            }
            else{
                $user->is_admin = false;
                $user->update();
            }
            $user_code = new VerificationCodeModel;
            $user_code->code = Str::random(80);
            $user_code->save();

            event(new WelcomeMailEvent($user));
            return redirect()->route('home.dashboard')->with('success',"The user {$user->name} was successfully updated");
    }

    public function deleteUser($id){
        $user = User::find($id);
        $user->delete();

        return redirect()->route('home.dashboard')->with('success',"The user {$user->name} was successfully deleted");
    }

    public function verifyUser($user){
        $user_old = User::where('id',$user->id)->first();
        $user_code = VerificationCodeModel::where('user_id',$user->id);
        $expired = now()->subHours(24);

        if ($user_old->email == $user->email && $user_code->code == $code && $user_code->created_at > $expired) {
            $verify = new User;
            $verify->email_verified_at = now()->format('Y-m-d H:i:s');  
        }
        elseif ($user_old->email != $user->email || $user_code->code != $code) {
            abort(401);
        }
        elseif ($user_code->created_at < $expired) {
           return view('dashboard.resendcode',['id'=>$user->id]);
        }
    }

    public function resendCode($id){
        $code = VerificationCodeModel::where('user_id',$id)->first();

        $code->delete();
        $code->user_id = $id;
        $code->code = Str::random(80);
        $code->save();

        $user = VerificationCodeModel::where('user_id',$id)->first();
        event(new WelcomeMailEvent($user));

        return redirect()->route('home')->with('success','New verification code was sended please chek your email address');
    }
}
