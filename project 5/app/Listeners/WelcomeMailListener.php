<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use App\Mail\NewUserWelcomeMail;

class WelcomeMailListener
{
    

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // dd('tuka');
        Mail::to($event->user->email)->send(

            new NewUserWelcomeMail($event->user)
        );
    }
}
