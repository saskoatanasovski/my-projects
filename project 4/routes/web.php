<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;



Route::get('/',[App\Http\Controllers\HomeController::class, 'landing'])->name('welcome');
Route::get('/guest',[App\Http\Controllers\GuestController::class, 'index'])->name('guest')->middleware('auth');

Route::middleware('auth', 'admin')->group(function () {
    Route::get('/admin/teams',[App\Http\Controllers\AdminTeamsController::class, 'teamsMain'])->name('teams.main');
    Route::get('/admin/teams/add',[App\Http\Controllers\AdminTeamsController::class, 'teamsAdd'])->name('teams.add');
    Route::get('/admin/teams/{id}/update',[App\Http\Controllers\AdminTeamsController::class, 'teamsUpdate'])->name('teams.update');
    Route::post('/admin/teams/{id}/update/submit',[App\Http\Controllers\AdminTeamsController::class, 'teamsUpdateSubmit'])->name('teams.update.submit');
    Route::post('/admin/teams/submit',[App\Http\Controllers\AdminTeamsController::class, 'teamsSubmit'])->name('teams.submit');
    Route::get('/admin/teams/{id}/delete',[App\Http\Controllers\AdminTeamsController::class, 'teamsDelete'])->name('teams.delete');

    Route::get('/admin/matches',[App\Http\Controllers\AdminMatchesController::class, 'matchesMain'])->name('matches.main');
    Route::get('/admin/matches/add',[App\Http\Controllers\AdminMatchesController::class, 'matchesAdd'])->name('matches.add');
    Route::get('/admin/matches/{id}/update',[App\Http\Controllers\AdminMatchesController::class, 'matchesUpdate'])->name('matches.update');
    Route::post('/admin/matches/submit',[App\Http\Controllers\AdminMatchesController::class, 'matchesSubmit'])->name('matches.submit');
    Route::post('/admin/matches/{id}/update/submit',[App\Http\Controllers\AdminMatchesController::class, 'matchesUpdateSubmit'])->name('matches.update.submit');
    Route::get('/admin/matches/{id}/delete',[App\Http\Controllers\AdminMatchesController::class, 'matchesDelete'])->name('matches.delete');

    Route::get('/admin/players',[App\Http\Controllers\AdminPlayersControler::class, 'playersMain'])->name('players.main');
    Route::get('/admin/players/add',[App\Http\Controllers\AdminPlayersControler::class, 'playersAdd'])->name('players.add');
    Route::get('/admin/players/{id}/update',[App\Http\Controllers\AdminPlayersControler::class, 'playersUpdate'])->name('players.update');
    Route::post('/admin/players/{id}/update/submit',[App\Http\Controllers\AdminPlayersControler::class, 'playersUpdateSubmit'])->name('players.update.submit');
    Route::get('/admin/players/{id}/delete',[App\Http\Controllers\AdminPlayersControler::class, 'playersDelete'])->name('players.delete');
    Route::post('/admin/players/submit',[App\Http\Controllers\AdminPlayersControler::class, 'playersSubmit'])->name('players.submit');

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');