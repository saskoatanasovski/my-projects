<?php

namespace App\Console\Commands;

use App\Models\FudbalMatch;
use Illuminate\Console\Command;

class PutRezultsCommand extends Command
{
    
    protected $signature = 'match:rezult';
    protected $description = 'putting rezults for matches';
    public function handle()
    {
        $rezults = [
            '2-2',
            '1-1',
            '1-2',
            '2-1',
            '3-3',
            '3-2',
            '3-1',
            '1-3',
            '2-3'
        ];
        $matches = FudbalMatch::where('date',date("Y-m-d",strtotime("+1 day")))->get();


        foreach ($matches as $match) {
            $match->create([
                'rezult'=>array_rand($rezults)
            ]);
        }
        
    }
}
