<?php

namespace App\Http\Controllers;

use App\Models\FudbalClub;
use App\Models\Player;
use Illuminate\Http\Request;

class AdminPlayersControler extends Controller
{
   public function playersMain(){
       $players = Player::all();
        
       return view('admin.players.playersMain',['players'=>$players]);
   }

   public function playersAdd(){
       $clubs = FudbalClub::all();
       return view('admin.players.addPlayer',['clubs'=>$clubs]);
   }

   public function playersSubmit(Request $req){
        $req->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'date_of_birth'=>'required',
            'fudbal_club'=>'required|exists:fudbal_clubs,id',
        ]);

        Player::create([
            'first_name'=>$req->input('first_name'),
            'last_name'=>$req->input('last_name'),
            'date_of_birth'=>$req->input('date_of_birth'),
            'fudbal_club_id'=>$req->input('fudbal_club'),
        ]);

        return redirect()->route('players.main')->with('success','New player is successfully added');
   }

   public function playersUpdate($id){
       $player = Player::find($id);
       $clubs = FudbalClub::all();

       return view('admin.players.updatePlayer',['player'=>$player,'clubs'=>$clubs]);
   }

   public function playersUpdateSubmit(Request $req,$id){
        $req->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'date_of_birth'=>'required',
            'fudbal_club'=>'required|exists:fudbal_clubs,id',
        ]);
        
        $player = Player::find($id);

        Player::where('id',$id)->update([
            'first_name'=>$req->input('first_name'),
            'last_name'=>$req->input('last_name'),
            'date_of_birth'=>$req->input('date_of_birth'),
            'fudbal_club_id'=>$req->input('fudbal_club')
        ]);

        return redirect()->route('players.main')->with('success',"The player {$player->first_name} {$player->last_name} is successfully updated");
   }

   public function playersDelete($id){
        $player = Player::where('id',$id)->first();
        $player->delete();

        return redirect()->route('players.main')->with('success',"The player {$player->first_name} {$player->last_name} is successfully deleted");
   }
}
