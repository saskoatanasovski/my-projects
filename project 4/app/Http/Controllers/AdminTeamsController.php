<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FudbalClub;

class AdminTeamsController extends Controller
{
    public function teamsMain(){

        $teams = FudbalClub::all();
        
        return view('admin.teams.teamsMain',['teams'=>$teams]);
       
    }

    public function teamsAdd(){
        return view('admin.teams.addTeam');
    }

    public function teamsUpdate($id){
    
        $team = FudbalClub::find($id);
       
        return view('admin.teams.updateTeam',['team'=>$team]);

    }

    public function teamsUpdateSubmit(Request $req,$id){
        $req->validate([
            'name'=>'required',
            'founded_at'=>'required'
        ]);

        $team = FudbalClub::find($id);
           
        FudbalClub::where('id',$id)->update([
            'name'=>$req->input('name'),
            'founded_at'=>$req->input('founded_at'),
            
        ]);

        return redirect()->route('teams.main')->with('success',"The {$team->name} was successfully updated");
    }

    public function teamsDelete($id){
        $team = FudbalClub::where('id',$id)->first();
        $team -> delete();

        return redirect()->route('teams.main')->with('success',"The {$team->name} was successfully deleted");
    }

    public function teamsSubmit(Request $req){

        $req->validate([
            'name'=>'required',
            'founded_at'=>'required'
        ]);

        FudbalClub::create([
            'name'=>$req->input('name'),
            'founded_at'=>$req->input('founded_at')
        ]);

        return redirect()->route('teams.main')->with('success','New fudbal club is successfully added');
    }
}
