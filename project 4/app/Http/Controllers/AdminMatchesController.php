<?php

namespace App\Http\Controllers;

use App\Models\FudbalClub;
use App\Models\FudbalMatch;
use App\Models\PlayerMatch;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Match_;

class AdminMatchesController extends Controller
{
    public function matchesMain(){
        $matches = FudbalMatch::all();

        return view('admin.matches.matchMain',['matches'=>$matches]);
    }

    public function matchesAdd(){
        $clubs = FudbalClub::all();

        return view('admin.matches.addMatch',['clubs'=>$clubs]);
    }

    public function matchesSubmit(Request $req){

        $req->validate([
            'home_team'=>'required|exists:fudbal_clubs,name',
            'guest_team'=>'required|exists:fudbal_clubs,name',
            'date'=>'required'
        ]);

        FudbalMatch::create([
            'home_team'=>$req->input('home_team'),
            'guest_team'=>$req->input('guest_team'),
            'date'=>$req->input('date'),
            'rezult'=>''
        ]);
        
        // $match = FudbalMatch::where('home_team',$req->input('home_team'))->where('guest_team',$req->input('guest_team'))->first();
        // $fudbal_club_home = FudbalClub::where('name',$req->input('home_team'))->first();
        // $fudbal_club_guest = FudbalClub::where('name',$req->input('guest_team'))->first();
        // dd($fudbal_club_home->player->id);
        // foreach ($fudbal_club_home->player as $player) {
        //     PlayerMatch::create([
        //         'player_id'=>$player->id,
        //         'match_id'=>$match->id
        //         ]);
        // }

        // foreach ($fudbal_club_guest->player as $player) {
        //     PlayerMatch::create([
        //         'player_id'=>$player->id,
        //         'match_id'=>$match->id
        //         ]);
        // }
        
            
            
        return redirect()->route('matches.main')->with('success','New match is successfully added');
    }

    public function matchesUpdate($id){
        $match = FudbalMatch::find($id);
        $clubs = FudbalClub::all();
        return view('admin.matches.updateMatch',['clubs'=>$clubs,'match'=>$match]);
    }

    public function matchesUpdateSubmit(Request $req,$id){
        
        $req->validate([
            'home_team'=>'required|exists:fudbal_clubs,name',
            'guest_team'=>'required|exists:fudbal_clubs,name',
            'date'=>'required'
        ]);
        
        $match = FudbalMatch::find($id);
           
        FudbalMatch::where('id',$id)->update([
            'home_team'=>$req->input('home_team'),
            'guest_team'=>$req->input('guest_team'),
            'date'=>$req->input('date'),
            'rezult'=>$req->input('rezult')
        ]);
        
        return redirect()->route('matches.main')->with('success',"The match between {$match->home_team} and {$match->guest_team} was successfully updated");
    }

    public function matchesDelete($id){
        $match = FudbalMatch::where('id',$id)->first();
        $match->delete();

        return redirect()->route('matches.main')->with('success',"The match between {$match->home_team} and {$match->guest_team} was successfully deleted");
    }
}
