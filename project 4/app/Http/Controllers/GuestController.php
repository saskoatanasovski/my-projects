<?php

namespace App\Http\Controllers;

use App\Models\FudbalMatch;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function index(){
        $matches = FudbalMatch::all();
        return view('guest',['matches'=>$matches]);
    }
}
