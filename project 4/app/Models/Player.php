<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use HasFactory;
    protected $fillable = ['first_name','last_name','date_of_birth','fudbal_club_id'];
    
    public function fudbalClub(){
        return $this->belongsTo(FudbalClub::class);
    }
}
