<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FudbalClub extends Model
{
    use HasFactory;
    protected $fillable = ['name','founded_at'];

    public function player(){
        return $this->hasMany(Player::class);
    }
}
