<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FudbalMatch extends Model
{
    use HasFactory;
    protected $fillable = ['date','home_team','guest_team','rezult'];
}
