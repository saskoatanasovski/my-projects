<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class PlayerMatch extends Pivot
{
   protected $fillable = ['player_id','match_id'];
}
