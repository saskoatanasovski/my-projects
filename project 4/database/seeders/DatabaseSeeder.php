<?php

namespace Database\Seeders;

use App\Models\FudbalClub;
use App\Models\Player;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
           'user_type'=>'admin',
           'name'=>'Administrator',
           'email'=>'administrator@mail.com',
           'password'=>Hash::make('password') 
        ]);
        
        for ($i=0; $i < 11; $i++) { 
            $faker = \Faker\Factory::create();
            FudbalClub::create([
                'name'=>$faker->word(2),
                'founded_at'=>$faker->dateTimeThisCentury(),
            ]);
        }
        for ($i=0; $i < 11; $i++) { 
            $faker = \Faker\Factory::create();
            Player::create([
                'first_name'=>$faker->firstName(),
                'last_name'=>$faker->lastName(),
                'date_of_birth'=>$faker->dateTimeBetween('-30 years','now')->format('Y-m-d'),
                'fudbal_club_id'=>$faker->numberBetween(1,5)
            ]);
        }
    }
}
