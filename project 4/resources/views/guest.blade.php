@extends('layouts.app')
@section('title','guest')
@section('content')
<div class="row">
    <div class="col-md-8 offset-md-2">
        <table class="table table-striped mt-5 table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Date</th>
                    <th scope="col">Home Team</th>
                    <th scope="col">Guest Team</th>
                    <th scope="col">Rezult</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($matches as $match)

                <tr>
                    <th scope="row"></th>
                    <td>{{ $match->date }}</td>
                    <td>{{ $match->home_team }}</td>
                    <td>{{ $match->guest_team }}</td>
                    <td>{{ $match->rezult }}</td>
                   
                </tr>
                    
                @endforeach
            </tbody>
        </table>

    </div>
</div>

@endsection
