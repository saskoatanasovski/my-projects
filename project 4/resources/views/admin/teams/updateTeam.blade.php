@extends('layouts.app')
@section('title','Update Team')
@section('content')

<div class="row mt-5">
    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-header">
             Update Team
            </div>
            <div class="card-body">
                <form action="{{ route('teams.update.submit', $team->id) }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $team->name }}">
                        @error("name")
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="founded_at">Founded At</label>
                        <input type="text" class="form-control" id="founded_at" name="founded_at" placeholder="Founded At (Y-M-D)" value="{{ $team->founded_at }}">
                        @error("founded_at")
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                   
                    <button type="submit" class="btn btn-success">Save</button>
                </form>
            </div>
          </div>
        
    </div>
</div>
@endsection
