@extends('layouts.app')
@section('title','Teams')
@section('content')
    
    
    <div class="row">
        <div class="col-md-8 offset-md-2 mt-5 ">
            @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{Session::get('success')  }}
                </div>
            @endif
            <a class="btn btn-primary float-right" href="{{ route('teams.add') }}">Add new Team</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <table class="table table-striped mt-5 table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Fudbal Club</th>
                        <th scope="col">Founded At</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($teams as $team)
    
                    <tr>
                        <th scope="row">{{ $team->id }}</th>
                        <td>{{ $team->name}}</td>
                        <td>{{ $team->founded_at }}</td>
                        <td><a class="" href="{{ route('teams.update', $team->id) }}">Edit</a> <a class="" href="{{ route('teams.delete', $team->id) }}">Delete</a></td>
                    </tr>
                        
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection