@extends('layouts.app')
@section('title','Matches')
@section('content')
<div class="row">
    <div class="col-md-8 offset-md-2 mt-5">
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{Session::get('success')  }}
            </div>
        @endif
        <a class="btn btn-primary float-right" href="{{ route('matches.add') }}">Add new Match</a>
    </div>
</div>
<div class="row">
    <div class="col-md-8 offset-md-2">
        <table class="table table-striped mt-5 table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Date</th>
                    <th scope="col">Home Team</th>
                    <th scope="col">Guest Team</th>
                    <th scope="col">Rezult</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($matches as $match)
               
                <tr>
                    <th scope="row">{{ $match->id }}</th>
                    <td>{{ $match->date }}</td>
                    <td>{{ $match->home_team }}</td>
                    <td>{{ $match->guest_team }}</td>
                    <td>{{ $match->rezult }}</td>
                    <td>
                        <a href="{{ route('matches.update',$match->id) }}">Edit</a> <a href="{{ route('matches.delete',$match->id) }}">Delete</a></td>
                
                </tr>
                    
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@endsection