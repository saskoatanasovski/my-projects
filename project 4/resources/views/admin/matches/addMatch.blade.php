@extends('layouts.app')
@section('title','Add Match')
@section('content')
<div class="row mt-5">
    <div class="col-md-8 offset-md-2">
      
        <div class="card">
            <div class="card-header">
             Create New Match
            </div>
            <div class="card-body">
                <form action="{{ route('matches.submit') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="home_team">Home Team</label>
                        <select class="form-control" id="home_team" name="home_team">
                            <option selected disabled>Home</option>
                            @foreach ($clubs as $club)
                                <option value="{{ $club->name }}">{{ $club->name }}</option>
                            @endforeach
                        </select>
                        @error("home_team")
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="guest_team">Gueast Team</label>
                        <select class="form-control" id="guest_team" name="guest_team">
                            <option selected disabled>Guest</option>
                            @foreach ($clubs as $club)
                                <option value="{{ $club->name }}">{{ $club->name }}</option>
                            @endforeach
                        </select>
                        @error("guest_team")
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="date">Date</label>
                        <input type="text" class="form-control" id="date" name="date" placeholder="Date" value="{{ old('date') }}">
                        @error("date")
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    
                   
                    <button type="submit" class="btn btn-success">Save</button>
                </form>
            </div>
        </div>
        
    </div>
</div>
@endsection