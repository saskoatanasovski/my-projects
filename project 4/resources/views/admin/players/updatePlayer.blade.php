@extends('layouts.app')
@section('title','Update Player')
@section('content')
    <div class="row">
        <div class="col-md-8 offset-md-2 mt-5">
            <div class="card">
                <div class="card-header">
                Update Player
                </div>
                <div class="card-body">
                    <form action="{{ route('players.update.submit',$player->id) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="first_name"> First Name</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{ $player->first_name }}">
                            @error("first_name")
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="last_name"> Last Name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="First Name" value="{{ $player->last_name }}">
                            @error("last_name")
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="date_of_birth">Date of birth</label>
                            <input type="text" class="form-control" id="date_of_birth" name="date_of_birth" placeholder="Date of birth (Y-M-D)" value="{{ $player->date_of_birth }}">
                            @error("date_of_birth")
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="fudbal_club">Fudbal Club</label>
                            <select class="form-control" id="fudbal_club" name="fudbal_club">
                                <option selected disabled>Please select a fudbal club</option>
                                @foreach ($clubs as $club)
                                    <option value="{{ $club->id }}" {{ $club->id == $player->fudbal_club_id ? 'selected' : '' }}>{{ $club->name }}</option>
                                @endforeach
                            </select>
                            @error("fudbal_club")
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    
                        <button type="submit" class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
@endsection