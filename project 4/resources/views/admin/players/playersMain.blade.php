@extends('layouts.app')
@section('title','Players')
@section('content')

    <div class="row">
        <div class="col-md-8 offset-md-2 mt-5">
            @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{Session::get('success')  }}
                </div>
            @endif
            <a class="btn btn-primary float-right" href="{{ route('players.add') }}">Add new Player</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <table class="table table-striped mt-5 table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Date of birth</th>
                        <th scope="col">Fudbal club</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($players as $player)
                   
                    <tr>
                        <th scope="row">{{ $player->id }}</th>
                        <td>{{ $player->first_name }}</td>
                        <td>{{ $player->last_name }}</td>
                        <td>{{ $player->date_of_birth }}</td>
                        <td>{{ $player->fudbalClub->name }}</td>
                        <td><a href="{{ route('players.update',$player->id) }}">Edit</a> <a href="{{ route('players.delete',$player->id) }}">Delete</a></td>
                    
                    </tr>
                        
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection