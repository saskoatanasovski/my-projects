<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .welcome-bg{
            background-image: url(https://cdn3.mozzartsport.com/upload/825x464/News/Image/2020_05/1588946143971_nasl.jpg);
            height: 90vh;
            background-size: cover;
            background-position: center;    
        }
        .welcome{
            background-color: rgba(0,0,0,0.8);
            height: 90vh;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light bg-light shadow-sm py-3">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            @if(Auth::user())
                            @if(Auth::user()->user_type=='guest')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('guest') }}">{{ __('Matches') }}</a>
                                </li>
                            @elseif(Auth::user()->user_type=='admin')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('matches.main') }}">{{ __('Matches') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('teams.main') }}">{{ __('Teams') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('players.main') }}">{{ __('Players') }}</a>
                                </li>
                            @endif
                        @endif
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav">
                            <!-- Authentication Links -->
                            @guest
                                @if (Route::has('login'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                @endif
                                
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }}
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>

            
                @yield('content')

        </div>
    </div>
</body>
</html>
