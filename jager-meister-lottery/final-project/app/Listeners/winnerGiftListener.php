<?php

namespace App\Listeners;

use App\Events\winnerGiftEvent;
use App\Mail\winnerGiftMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class winnerGiftListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        
        Mail::to($event->participient->email)->send(
            new winnerGiftMail($event->participient, 'admin-controller')
        );
    }
}
