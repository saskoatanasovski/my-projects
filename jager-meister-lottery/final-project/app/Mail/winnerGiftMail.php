<?php

namespace App\Mail;

use App\Models\Gifts;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class winnerGiftMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($participient)
    {
        $this->participient = $participient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       
        return $this->view('mails.winnerGiftMail', [
            
            'gift' => $this->participient->gift,
        ]);
    }
}
