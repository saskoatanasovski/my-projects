<?php

namespace App\Http\Controllers;

use App\Models\ContestData;
use App\Models\Gifts;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $participients = ContestData::where('status','no value')->get();
       
        $gifts = Gifts::all();
       
       
        return view('admin.home',['participients'=>$participients,'gifts'=>$gifts]);
   
    }
}
