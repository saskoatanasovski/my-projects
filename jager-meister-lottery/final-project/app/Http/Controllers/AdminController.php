<?php

namespace App\Http\Controllers;

use App\Events\winnerGiftEvent;
use App\Models\ContestData;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function approve(Request $req,$id){
       
        $req->validate([
            'gift'=>'required'
        ]);
        ContestData::where('id',$id)->update([
            'status'=>'approved',
            'gift'=>$req->input('gift')
        ]);
        $participient = ContestData::where('id',$id)->first();
       
        event(new winnerGiftEvent($participient));
        return redirect()->route('home');
        

    }
    public function decline($id){

        ContestData::where('id',$id)->update([
            'status'=>'declined'
        ]);
        return redirect()->route('home');
    }
    public function approvedBill(){
        $participients = ContestData::where('status','approved')->get();
        if ($participients) {

            return view('admin.approvedBill',['participients'=>$participients]);
      
        } else {
            return redirect()->route('admin.approved-bill')->with('msg','There is no bills for review.');
        }
        
    }

    public function declinedBill(){
        $participients = ContestData::where('status','declined')->get();
        return view('admin.declinedBill',['participients'=>$participients]);
    }
    public function account(){
        $user = Auth::user();

        return view('admin.accountHome',['user'=>$user]);
    }

    public function edit($id){

        $user = User::where('id',$id)->first();
        return view('admin.accountEdit',['user'=>$user]);

    }

    public function update(Request $req,$id){
        $req->validate([
            'name'=>'required|string|max:255',
            'email'=>'required|string|email|unique:users|max:255',
            'oldPassword'=>'required|string|min:4',
            'newPassword'=>'required|string|min:4',
            'confirmPassword'=>'required|string|min:4',
        ]);
        $user = User::where('id',$id)->first();
       

        if ( Hash::check($req->input('oldPassword'),$user->password)) {

        }else {
            
            return redirect()->route('account-edit',$user->id)->with('msg',"Your input doesn't match with your password");
        }

        if ($req->input('newPassword') === $req->input('confirmPassword')) {

        }else {
            
            return redirect()->route('account-edit',$user->id)->with('confpass',"Your input doesn't match with your new password");
        }

        $user->update([
            'name'=>$req->input('name'),
            'email'=>$req->input('email'),
            'password'=>Hash::make($req->input('confirmPassword'))
        ]);
        return redirect()->route('account')->with('success','Your account was successfully updated');
    }
}
