<?php

namespace App\Http\Controllers;

use App\Models\ContestData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ApiBillController extends Controller
{
    public function store(Request $req){
        
        $req->validate( [
            'file'=>'file|filled|image',
            'email'=>'required|string|unique:contest_data|email'
        ]);
       
        $file = $req->file('file');
        $newFile = time().$file->getClientOriginalName();
        $destination = public_path(('/images'));
        $file->move($destination,$newFile);
       
    
        ContestData::create([
            'email'=>$req->input('email'),
            'bill'=>$newFile,
            'bill_text'=>$req->input('text'),
            'api_info'=>$req->input('img_status'),
            
        ]);
       
        return response()->json([
           'success'=>'You successfully aplied for the Jagermeister lottery. Thank you for your participation in this contest further info you will get on your email address.',
           'image'=>$newFile
        ]);
    
    }

    public function update(Request $req){
       
        ContestData::where('email',$req->input('email'))->update([
            'bill_text'=>$req->input('text'),
            'api_info'=>$req->input('imgStatus')
        ]);

    }

    public function delete(Request $req){
        
      
        $path = "/images/".$req->input('image');
        
        ContestData::where('email',$req->input('email'))->delete();
    
        $deleted = File::delete(public_path($path));

       
    }
    public function api(){
        $faker = \Faker\Factory::create();

        $data = [
            rand(0,2),
            $faker->words(50,true)
        ];
        
        return response()->json($data);
    }
}
