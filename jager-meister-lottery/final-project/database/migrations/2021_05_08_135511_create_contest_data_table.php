<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContestDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contest_data', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('bill');
            $table->text('bill_text')->nullable();
            $table->string('api_info')->nullable();
            $table->string('status')->default('no value');
            $table->string('gift')->default('no value');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contest_data');
    }
}
