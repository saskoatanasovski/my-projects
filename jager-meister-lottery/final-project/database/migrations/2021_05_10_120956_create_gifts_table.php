<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifts', function (Blueprint $table) {
            $table->id();
            $table->string('gift_name');
            $table->timestamps();
        });

        DB::table('gifts')->insert([
            ['gift_name' => 'hat'],
            ['gift_name' => 'glasses'],
            ['gift_name' => 't-shirt'],
            ['gift_name' => 'torch'],
            ['gift_name' => 'mp3player'],
            
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gifts');
    }
}
