<?php

use App\Http\Controllers\ApiBillController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('bill/store', [ApiBillController::class,'store'])->name('bill.store');
Route::post('bill/update', [ApiBillController::class,'update'])->name('bill.update');
Route::post('bill/delete', [ApiBillController::class,'delete'])->name('bill.delete');
Route::get('bill/api', [ApiBillController::class,'api'])->name('bill.api');