<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[App\Http\Controllers\UserPanelController::class, 'index'])->name('user-panel');
Route::post('/store',[App\Http\Controllers\UserPanelController::class, 'store'])->name('bill-store');

Auth::routes();
Route::middleware('auth', 'admin')->group(function (){

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/approved', [App\Http\Controllers\AdminController::class, 'approvedBill'])->name('approved-bill');
    Route::get('/declined', [App\Http\Controllers\AdminController::class, 'declinedBill'])->name('declined-bill');
    Route::get('/account', [App\Http\Controllers\AdminController::class, 'account'])->name('account');
    Route::get('/account/{id}/edit', [App\Http\Controllers\AdminController::class, 'edit'])->name('account-edit');
    Route::post('/account/{id}/update', [App\Http\Controllers\AdminController::class, 'update'])->name('account-update');
    Route::get('/{id}/decline', [App\Http\Controllers\AdminController::class, 'decline'])->name('decline');
    Route::post('/{id}/approve', [App\Http\Controllers\AdminController::class, 'approve'])->name('approve');
});
