const { default: axios } = require('axios');
const { sum, last } = require('lodash');

require('./bootstrap');
let allBills = $('#all');
let definitlyBils = $('#bills');
let possibleBills = $('#possibleBills');
let definitlyCard = $('.img_status1');
let possibleCard = $('.img_status2');
let checkedRadio = sessionStorage.getItem('radioInput');
let content = $('#content');
let blockedUser = localStorage.getItem('blocked-user');
let progressBar = document.getElementById('progressBar');
let mainSubmitBtn = $('#mainSubmitBtn');
let progressContainer = document.getElementById('progress-container');
let lastclear = parseInt(localStorage.getItem('lastclear')),
time_now  = (new Date().getTime());

if(blockedUser){
   
    if (( time_now -lastclear ) < 1000 * 60 * 60 * 24) {
        
        content.html(`
                <div class="alert alert-danger" role="alert">
                    <h3 class"text-success">You are not allowed to access this page because you entered tree times file which is not a bill.</h3>
                </div>
                `)
            
    }
    else{
        
        localStorage.clear();
        sessionStorage.clear();
        localStorage.setItem('lastclear', time_now);
    }
}


if (checkedRadio == 'all') {
    allBills.attr('checked',true);
    definitlyCard.css('display','block');
    possibleCard.css('display','block');

} else if(checkedRadio == 'bills'){
    definitlyBils.attr('checked',true);
    definitlyCard.css('display','block');
    possibleCard.css('display','none');
}
else if(checkedRadio == 'possibleBills'){
    possibleBils.attr('checked',true);
    definitlyCard.css('display','none');
    possibleCard.css('display','block');
}

// "START" saving data from user panel
let headers = {
    'Content-Type': 'apication/json',
    'Accept': 'application/json'
}
let billForm = $('#billForm');

billForm.on('submit',function(e){
    e.preventDefault();
    let email = $('#email').val();
    let file = $('#file');
    let text = $('#errText');
    let mailErr = $('#emailEr');
    let fileErr = $('#fileEr');
   
    if (!email) {
        mailErr.text('Please enter your email address');
        $('#staticBackdrop').modal('show');
    }
    else if(file[0].files.length === 0){
        fileErr.text('Please choose a file');
        $('#staticBackdrop').modal('show');
    }
    else
    {
       
        let formData = new FormData();
        formData.append('email',email);
        formData.append('file',file[0].files[0]);
        axios.post('/api/bill/store/', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            onUploadProgress: (ProgressEvent)=>{
               
                let percentCompleted = Math.round( (ProgressEvent.loaded * 100) / ProgressEvent.total );

                mainSubmitBtn.on('click',function(e){
                    progressContainer.style.display = 'block';  
                })
                let persentage = percentCompleted + '%'
                progressBar.style.width = persentage;
                progressBar.style.height = '100%';
                progressBar.innerText = persentage;

                
          
            }
        })
        .then(function(response){
            
           
            let image = response.data.image;
            let success = response.data.success;
            
            fetch(`/api/bill/api`)
        
            .then(function(response){
                console.log(response);
                return response.json();
            })
            .then(function(data){
                 console.log(data);

                if (data[0] == '0') {
                    let sendBillBtn = $('#sendBill');
                    sendBillBtn.on('click',function(e){
                        let counterSession = sessionStorage.getItem('counterSession');
                        sessionStorage.setItem('counterSession',++counterSession );
                       
                    })
                    let counterSession = sessionStorage.getItem('counterSession');
                       
                        if(counterSession > 2){
                            localStorage.setItem('blocked-user',email);
                                content.html(`
                                <div class="alert-danger" role="alert">
                                    <h3 class"text-success">You've entered three times files wich are not bills, therefor you will block to take part of the Jagermeister lottery for 24 hours</3>
                                </div>
                                `)
                        }
                    localStorage.setItem('lastclear', time_now);
                    let formData = new FormData();
                    formData.append('image',image);
                    formData.append('email',email);
                    formData.append('msg','notBill');

                    axios.post('/api/bill/delete/', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then(function(response){
                        
                    })
                    .catch(function(err){
                        console.log(err);
                    })
                    $('#staticBackdrop').modal('hide');
                    text.html(`
                        <div class="alert alert-danger" role="alert">
                            <h4 class'text-danger'>Sorry the file you just entered is not a bill! Please try again.</h4>
                        </div>
                    `)

                }else if(data[0] == '1'){
                    let formData = new FormData();
                    formData.append('email',email);
                    formData.append('imgStatus',data[0]);
                    formData.append('text',data[1]);
                    formData.append('msg','bill');
                    axios.post('/api/bill/update/', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                    })
                    .then(function(response){
                        $('#staticBackdrop').modal('hide');
                        content.html(`
                            <div class="alert alert-success" role="alert">
                                <h3 class"text-success mb-3">${success}</3>
                            </div>
                            <a href="/" class=" btn btn-primary">Go back to home page</a>
                        `)
                        $('#email').val('');
                        $('#file').val('');
                        localStorage.clear();
                        sessionStorage.clear();
                    })
                    .catch(function(err){
                        console.log(err);
                    }) 
                        
                }else if (data[0] == '2') {
                    let formData = new FormData();
                    formData.append('email',email);
                    formData.append('imgStatus',data[0]);
                    formData.append('text',data[1]);
                    formData.append('msg','possible bill');
                    axios.post('/api/bill/update/', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then(function(response){
                        $('#staticBackdrop').modal('hide');
                        content.html(`
                            <div class="alert alert-success" role="alert">
                                <h3 class"text-success">${success}</3>
                            </div>
                            <a href="/" class=" btn btn-primary">Go back to home page</a>
                        `)
                        $('#email').val('');
                        $('#file').val('');
                        localStorage.clear();
                        sessionStorage.clear();
                    })
                    .catch(function(err){
                        console.log(err);
                    })    
                }
                progressContainer.style.display = 'none'
            })
            .catch(function(err){
                console.log(err);

            })

           
        })
        .catch(function(err){
            console.log('ep', err.response);
            let mail = document.querySelector('#emailEr');
            let fileErr = document.querySelector('#fileEr');
            let email = err.response.data.errors?.email;
            email = email && email.length ? email[0] : null;
            

            let file = err.response.data.errors?.file;
            file = file && file.length ? file[0] : null
            
           
            if (file) {
                 fileErr.innerHTML = `<p>${file}</p>`;
            }else if(email){
                mail.innerHTML = `<p>${email}</p>`;
            }
           
        })  

        
    }  
})
// "END" saving data from user panel
// "START" selecting bils in admin panel "definitly bils" and "possible bils"

allBills.on('click',function(e){
    definitlyCard.css('display','block');
    possibleCard.css('display','block');
    sessionStorage.removeItem('radioInput');
    sessionStorage.setItem('radioInput','all');
})


definitlyBils.on('click',function(e){
    definitlyCard.css('display','block');
    possibleCard.css('display','none');
    sessionStorage.removeItem('radioInput');
    sessionStorage.setItem('radioInput','bills');
})

possibleBills.on('click',function(e){
    definitlyCard.css('display','none');
    possibleCard.css('display','block');
    sessionStorage.removeItem('radioInput');
    sessionStorage.setItem('radioInput','possibleBills');
})
// "END" selecting bils in admin panel "definitly bils" and "possible bils"
// "START" approving bills
let approveBtns = document.querySelectorAll('.approveBtn')
let cardImg = document.querySelectorAll('.card-image');
let modalImg = document.querySelector('#modalImg');
let cardForm = document.querySelector('#cardForm');
let giftErr = document.querySelector('#giftErr');

let giftSubmit = document.querySelector('#giftSubmit');
approveBtns.forEach(btn=> {
    btn.addEventListener('click',function(e){
        let id = e.target.dataset.id;
        cardForm.setAttribute('action','/'+id+'/approve')
        
    })
});
cardImg.forEach(img=>{
    img.addEventListener('click',function(e){
        let imgSrc = e.target.dataset.img;
        modalImg.setAttribute('src',imgSrc)
    })
})
giftSubmit.addEventListener('click',function(e){
    let gift = document.querySelector('#gift');
    
    
    if (gift.value == '') {
        e.preventDefault();
        $('#modal-admin').modal('show');
        giftErr.innerText = 'The gift field is required';
    } else {
        $('#modal-admin').modal('hide');
    }
})

// "END" approving bills