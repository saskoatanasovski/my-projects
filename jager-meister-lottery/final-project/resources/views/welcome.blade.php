<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script>let base_url ="{{ env('APP_URL') }}" </script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Jagermeister </title>
</head>

<body>
    <div class="cover-photo">
        <div class="background">
            <div class="container-fluid">
                <div class="row p-5">
                    <div class="col">
                        <a href="{{ route('home') }}" class="text-white float-right  h5">Go to admin panel <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="row">
                   
                    <div id="content" class="col contnent text-white d-flex justify-content-center flex-column align-items-center">
                        <h3>Welcome to jagermeister lottеry</h3>
                        <p>To take part in this contest you have to send us a scan or photo from a bill in wich you have
                                any product of jagermaister.</p>
                        <div id="errText">
                                
                        </div>
                        <button id="sendBill" type="button" class="btn btn-success" data-toggle="modal" data-target="#staticBackdrop">
                            Send bill
                        </button>
                        
                    </div>
                   
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade " id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header  modal-bg">
                    <h5 class="modal-title" id="staticBackdropLabel">Jagermeister </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body  modal-bg">
                    <form id="billForm"  enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="email">Please enter your mail:</label>
                            <input type="email" class="form-control mb-3" id="email" name="email" placeholder="examle@mail.com">
                        </div>
                            <div id="emailEr" class="text-danger"></div>
                        <div class="form-group">
                            <label for="file">Please enter your bill:</label><br>
                            <input type="file" id="file" name="file">
                        </div>
                            <div id="fileEr" class="text-danger"></div>
                        <div id="progress-container" class="progress-container mb-3 ">
                            <div id="progressBar" class="progressBar"></div>
                        </div>
                        <button id="mainSubmitBtn" type="submit" class="btn btn-primary">Submit</button>
                    </form>
                   
                </div>
                
            </div>
        </div>
    </div>



   
</body>

</html>
