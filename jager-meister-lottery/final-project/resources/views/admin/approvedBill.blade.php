@extends('layouts.app')
@section('title','Approved Bills')
@section('content')
    <div class="container-fluid py-5">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="row row p-2 p-md-0">
                    @if (count($participients))
                        @foreach ($participients as $participient)
                        <div class="col-md-3 mb-5 ">
                            <div class="card modal-bg" style="height: 25rem;">
                                <h3 class="card-title text-success bg-white m-0 p-1 text-capitalize">{{ $participient->status }}</h3>
                                <h4 class="card-subtitle font-weight-bold text-dark bg-white m-0 p-1">Gift:{{ $participient->gift }}</h4>
                                <img src="/images/{{ $participient->bill }}" class="card-img-top card-image" alt="bill">
                                <div class="card-body overflow-auto">
                                    <h5>{{ $participient->email }}</h5>
                                    <p class="card-text overflow-auto">{{ $participient->bill_text }}</p>
                                </div>
                                
                            </div>
                        </div>
                        @endforeach
                    @else
                        <div class="col">
                            <div class="alert alert-warning d-flex justify-content-center align-items-center" role="alert">
                                <h1>There is no approved bills.</h1>
                            </div>
                        </div>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
@endsection