@extends('layouts.app')
@section('title','Account Edit')
@section('content')

<div class="container-fluid account">
    <div class="row account pt-4">
        <div class="col-md-6 offset-md-3 d-flex justify-content-center  flex-column ">
            <h2 class="text-center my-4 font-weight-bold text-white">Update your account</h2>
            <form class="modal-bg p-5 rounded" action="{{ route('account-update',$user->id) }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="email">Username</label>
                    <input type="name" class="form-control" id="name" name="name" value="{{ $user->name }}">
                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                    @error('email')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="oldPassword">Old Password</label>
                    <input type="password" class="form-control" id="oldPassword" name="oldPassword" value="{{ old('oldPassword') }}">
                    @if (Session::has('msg'))
                        <div class="text-danger">{{ Session::get('msg') }}</div>
                    @endif
                    @error('oldPassword')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                   
                </div>
                <div class="form-group">
                    <label for="newPassword">New Password</label>
                    <input type="password" class="form-control" id="newPassword" name="newPassword" value="{{ old('newPassword') }}">
                    @error('newPassword')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="confirmPassword">Confirm Password</label>
                    <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" value="{{ old('confirmPassword') }}">
                    @error('confirmPassword')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                    @if (Session::has('confpass'))
                        <div class="text-danger">{{ Session::get('confpass') }}</div>
                    @endif
                </div>
                
                <button type="submit" class="btn btn-success">Update</button>
            </form>

        </div>
    </div>
</div>

@endsection
