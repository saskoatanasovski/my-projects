@extends('layouts.app')
@section('title','Admin')
@section('content')

<div class="container-fluid ">
    <div class="row ">
        <div class="col-md-10 offset-md-1">
            <div class="row p-4 p-md-0">
                <div class="col-md-10 offset-md-1 py-md-5">
                    
                        <input class="form-check-input mr-5" type="radio" name="exampleRadios" id="all" >
                        <label class="form-check-label text-white h5 mr-5" for="all">
                           All Bills
                        </label>
                        <input class="form-check-input mr-5" type="radio" name="exampleRadios" id="bills">
                        <label class="form-check-label text-white h5 mr-5" for="bills">
                            Definitely Bills
                        </label>
                        <input class="form-check-input mr-5" type="radio" name="exampleRadios" id="possibleBills">
                        <label class="form-check-label text-white h5" for="possibleBills">
                            Possible Bills
                        </label>
                  
                </div>
            </div>
            <div class="row p-2 p-md-0">
              
                @if (count($participients))
                    @foreach ($participients as $participient)
                    <div class="col-md-3 mb-5 img_status{{ $participient->api_info }}">
                        <div class="card modal-bg " >
                            <img data-img="/images/{{ $participient->bill }}" src="/images/{{ $participient->bill }}" class="card-img-top card-image" alt="bill" data-toggle="modal" data-target="#cardImageModal">
                            <div class="card-body overflow-auto" style="height: 10rem;">
                                <h5>{{ $participient->email }}</h5>
                                <p class="card-text">{{ $participient->bill_text }}</p>
                            </div>
                            <div class="card-footer">
                                <button data-id="{{ $participient->id }}" type="button" class="btn btn-success approveBtn mb-2"
                                    data-toggle="modal" data-target="#modal-admin">
                                    Approve
                                </button>
                                <a href="{{ route('decline',$participient->id) }}" class="btn btn-danger mb-2">Decline</a>
                            </div>
                        </div>
                    </div>
                    @endforeach                  
                @else
                    <div class="col">
                        <div class="alert alert-warning d-flex justify-content-center align-items-center" role="alert">
                            <h1>There is no bills to be reviewed.</h1>
                        </div>
                    </div>
                @endif
               
            </div>
        </div>
        <div class="modal fade " id="modal-admin" data-backdrop="static" data-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header  modal-bg">
                        <h5 class="modal-title" id="staticBackdropLabel">Gifts</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body  modal-bg">
                        <form id="cardForm"  method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="gift">Choose a gift</label>
                                <select multiple class="form-control" id="gift" name="gift">
                                    @foreach ($gifts as $gift)
                                    <option value="{{ $gift->gift_name }}">{{ $gift->gift_name }}</option>
                                    @endforeach
                                </select>
                               
                                <div id="giftErr" class="text-danger"></div>
                                
                            </div>

                            <button id="giftSubmit" type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="cardImageModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body gallery">
                                <img id="modalImg" src="" class="w-100">
                            </div>
                        </div>
                    </div>
                </div>
    </div>
</div>
@endsection
