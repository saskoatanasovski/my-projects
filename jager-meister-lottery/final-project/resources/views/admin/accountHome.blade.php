@extends('layouts.app')
@section('title','Account Home')
@section('content')

    <div class="container-fluid account">
        <div class="row account">
            <div class="col-md-6 offset-md-3 d-flex justify-content-center  flex-column ">
                @if (Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <h3>{{ Session::get('success') }}</h3>
                    </div>
                @endif
                <div class="card modal-bg text-white">
                    <div class="card-header">
                        <h4 class="card-title mb-0">Your Account data</h4>
                    </div>
                    <div class="card-body">
                        
                        <h6><strong>User Name</strong>: {{ $user->name }}</h6>
                        <h6><strong>Email</strong>: {{ $user->email }}</h6>
                   
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-primary" href="{{ route('account-edit',$user->id) }}">Update your account</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
