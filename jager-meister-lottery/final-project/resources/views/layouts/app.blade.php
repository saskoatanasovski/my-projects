<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    
    <script>let base_url ="{{ env('APP_URL') }}" </script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="admin-photo">
        <div class="background">
            <div id="app">
                <nav class="navbar navbar-expand-md navbar-dark navbar-bg shadow-sm">
                    <div class="container-fluid">
                        <a class="navbar-brand h4 mr-5" href="{{ route('user-panel') }}">
                            <img class="logo" src="{{ asset('/img/logo.png') }}" alt="logo">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>
        
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                            @if (Auth::user())
                                @if (Auth::user()->user_type = 'admin') 
                                    <ul class="navbar-nav text-white ml-3 h5">
                                        <li class="nav-item">
                                        <a class="nav-link {{ Route::currentRouteName() == 'home' ? 'active' : '' }}" href="{{ route('home') }}">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link {{ Route::currentRouteName() == 'approved-bill' ? 'active' : '' }}" href="{{ route('approved-bill') }}">Approved Receipts</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link {{ Route::currentRouteName() ==  'declined-bill' ? 'active' : '' }}" href="{{ route('declined-bill') }}">Declained Receipts</a>
                                    </li>
                                </ul>
                                @endif
                            @endif
                            <!-- Right Side Of Navbar -->
                            <ul class="navbar-nav ml-auto h5">
                                <!-- Authentication Links -->
                                @guest
                                    @if (Route::has('login'))
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </li>
                                    @endif
        
                                   
                                @else
                                    <li class="nav-item dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            {{ Auth::user()->name }}
                                        </a>
        
                                        <div class="dropdown-menu dropdown-menu-right modal-bg" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item text-light" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>
        
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                @csrf
                                            </form>
                                            
                                            <a class="dropdown-item text-light" href="{{ route('account') }}">Your Account</a>
                                            
                                        </div>
                                    </li>
                                @endguest
                            </ul>
                        </div>
                    </div>
                </nav>
                <main>
                    @yield('content')
                </main>
               
            </div>
        </div>
    </div>
   
    
</body>
</html>
